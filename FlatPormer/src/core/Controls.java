package core;

public class Controls {
	
	public void move() {

		// velocity build up & down
		if (upAccel) {
			yVel -= speed * 2;
		} else if (downAccel) {
			yVel += speed * 2;
		} else if (!upAccel && !downAccel) {
			yVel *= FRICTION;

		}

		// velocity build left & right
		if (leftAccel) {
			xVel -= speed;

		} else if (rightAccel) {
			xVel += speed;

		}
		if (!leftAccel && !rightAccel) {
			xVel *= FRICTION;
		}

		// friction builder
		if (!upAccel && !downAccel && !leftAccel && !rightAccel) {
			yVel *= FRICTION;
			xVel *= FRICTION;
		}

		// player speed cap
		if (yVel >= maxSpeed) {
			yVel = maxSpeed;
		} else if (yVel <= -maxSpeed) {
			yVel = -maxSpeed;
		}

		yPos += yVel;

		if (xVel >= maxSpeed) {
			xVel = maxSpeed;
		} else if (xVel <= -maxSpeed) {
			xVel = -maxSpeed;
		}
		xPos += xVel;

		// Moving bounds
		if (yPos <= 0) {
			yPos = 0;
		} else if (yPos >= Game.HEIGHT - ySize) {
			yPos = Game.HEIGHT - ySize;
			yVel = 0;

		}
		if (xPos <= 0) {
			xPos = 0;
		} else if (xPos >= Game.WIDTH - xSize) {
			xPos = Game.WIDTH - xSize;
			xVel = 0;
		}

	}

	public void setAccel(int dir, boolean input) {
		switch (dir) {
		case 1:
			upAccel = input;
			break;
		case 2:
			downAccel = input;
			break;
		case 3:
			leftAccel = input;
			break;
		case 4:
			rightAccel = input;
			break;
		}
	}
}

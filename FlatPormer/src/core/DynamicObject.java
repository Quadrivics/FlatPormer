package core;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Arrays;

public class DynamicObject extends Controls implements GameObject {
	private int xSize, ySize, xStartPos, yStartPos;
	private double xVel, yVel, xPos, yPos;
	private boolean upAccel, downAccel, leftAccel, rightAccel;

	boolean contact;
	int bumpCount;
	double speed = 0.2;
	int maxSpeed = 8;

	public DynamicObject(int xSize, int ySize, int xPos, int yPos) {
		setxSize(xSize);
		setySize(ySize);
		setxPos(xPos);
		setxStartPos(xPos);
		setyPos(yPos);
		setyStartPos(yPos);
	}

	public void draw(Graphics g) {
		g.setColor(new Color(255, 0, 0));
		g.fillRect((int) xPos, (int) yPos, xSize, ySize);

	}

	public void reset() {
		xPos = xStartPos;
		yPos = yStartPos;
	}

	public void checkPlayerCollision(StaticObject so) {
		// System.out.println("xPos: "+xPos+"| yPos: " +yPos);
		System.out.println("xPos: " + xPos + "| xWall: " + (so.getxPos()+so.getxSize()));
		if ((xPos >= so.getxPos() - xSize) & (xPos <= so.getxPos() + so.getySize())) {
			if (yPos <= so.getyPos()) {
	//			System.out.println("Boven");
				if ((yPos >= so.getyPos() - ySize)) {
					yVel = -yVel;
					yPos = yPos-10;
					bumpCount++;
				}
			} 
			if (yPos >= so.getyPos()) {
	//			System.out.println("Onder");
				if ((yPos <= so.getyPos() + so.getySize())) {
					yVel = -yVel;
					yPos = yPos+10;
					bumpCount++;

				}
			}
		}
		if ((yPos >= so.getyPos() - ySize) & (yPos <= so.getyPos() + so.getySize())) {
			if (xPos <= so.getxPos()) {
//				System.out.println("Links");
				if (xPos >= so.getxPos()-xSize) {
					xPos = xPos-10;
					xVel = -xVel;
					bumpCount++;
				}
			} 
			if (xPos >= (so.getxPos() + so.getxSize()-1)) {
//				System.out.println("Rechts");			
				if (xPos <= (so.getxPos() + so.getxSize())+1) {
					xPos = xPos+10;
					xVel = -xVel;
					bumpCount++;
				}
			}
		}

	}

	@Override
	public int getxSize() {
		return xSize;
	}

	@Override
	public int getySize() {
		return ySize;
	}

	@Override
	public double getxPos() {
		return xPos;
	}

	@Override
	public double getyPos() {
		return yPos;
	}

	@Override
	public void setxSize(int s) {
		xSize = s;
	}

	@Override
	public void setySize(int s) {
		ySize = s;
	}

	@Override
	public void setxPos(int p) {
		xPos = p;
	}

	@Override
	public void setyPos(int p) {
		yPos = p;
	}

	@Override
	public void setxStartPos(int p) {
		xStartPos = p;
	}

	@Override
	public void setyStartPos(int p) {
		yStartPos = p;
	}

	public boolean getUpAccel() {
		return upAccel;
	}

	public boolean getDownAccel() {
		return downAccel;
	}

	public boolean getLeftAccel() {
		return leftAccel;
	}

	public boolean getRightAccel() {
		return rightAccel;
	}

	public void setxVel(int v) {
		xVel = v;
	}

	public void setyVel(int v) {
		yVel = v;
	}
}

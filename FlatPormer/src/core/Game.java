package core;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Game extends Applet implements Runnable, KeyListener {
	static final long serialVersionUID = 19983683L;
	static final int WIDTH = 800, HEIGHT = 768;
	Thread thread;
	DynamicObject p1;
	Font font;
	StaticObject block;
	boolean gameStarted;
	Graphics gfx;
	Image img;

	public void init() {
		this.resize(WIDTH, HEIGHT);
		gameStarted = true;
		this.addKeyListener(this);
		p1 = new DynamicObject(40, 40, 50, 10);
		block = new StaticObject(200, 200, 200, 250 );
		img = createImage(WIDTH, HEIGHT);
		gfx = img.getGraphics();
		thread = new Thread(this);
		thread.start();
	}

	public void paint(Graphics g) {
		gfx.setColor(new Color(127,0,0));
		gfx.fillRect(0, 0, WIDTH, HEIGHT);
		if (gameStarted) {
			p1.draw(gfx);
			block.draw(gfx);
			gfx.setColor(Color.white);
			gfx.setFont(new Font("Monteserat", Font.PLAIN, 14));
			gfx.drawString(("BumpCount: " + p1.bumpCount), 480, 350);
			gfx.drawString(("p1 cor: x" + (int)p1.getxPos() + " y" + (int)p1.getyPos()), 480, 330);
			
			//System.out.println("xPos: " + p1.getxPos() + " | yPos: " + p1.getyPos() + " | Up: " + p1.getUpAccel() + " | Down: " + p1.getDownAccel() + " | Left: " + p1.getLeftAccel() + " | Right: " +  p1.getRightAccel());
		} else if (!gameStarted) {
			gfx.setColor(Color.white);
			gfx.setFont(new Font("Monteserat", Font.PLAIN, 14));
			gfx.drawString("Toets enter om spel te starten", 480, 350);
		}
		

		g.drawImage(img, 0, 0, this);
	}

	public void update(Graphics g) {
		paint(g);
	}

	public void run() {
		for (;;) {
			if (gameStarted) {
				p1.move();	
				p1.checkPlayerCollision(block);				
			}
			repaint();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void resetGame() {
		gameStarted = false;
		p1.reset();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			p1.setAccel(1, true);
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			p1.setAccel(2, true);
		} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			p1.setAccel(3, true);
		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			p1.setAccel(4, true);
		} else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			gameStarted = true;
		} else if (e.getKeyCode() == KeyEvent.VK_R) {
			resetGame();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			p1.setAccel(1, false);
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			p1.setAccel(2, false);
		} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			p1.setAccel(3, false);
		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			p1.setAccel(4, false);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
}

package core;

public interface GameObject {
	final double FRICTION = 0.98;
	final double GRAVITY = 0.96;
	public void setxSize(int s);
	public void setySize(int s);
	public void setxPos(int p);
	public void setyPos(int p);
	
	public void setxStartPos(int p);
	public void setyStartPos(int p);
		
	public int getxSize();
	public int getySize();
	public double getxPos();
	public double getyPos();

	public void reset();
	
}

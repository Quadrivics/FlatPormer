package core;

import java.awt.Color;
import java.awt.Graphics;

public class StaticObject implements GameObject {
	private int xSize, ySize, xStartPos, yStartPos;
	private double xPos, yPos;
	
	public StaticObject(int xSize, int ySize, int xPos, int yPos) {
		setxSize(xSize);
		setySize(ySize);
		setxPos(xPos);
		setxStartPos(xPos);
		setyPos(yPos);
		setyStartPos(yPos);
	}

	public void draw(Graphics g) {
		g.setColor(new Color(255, 0, 0));
		g.fillRect((int) xPos, (int) yPos, xSize, ySize);
	}

	public void reset() {
		xPos = xStartPos;
		yPos = yStartPos;
	}
	
	@Override
	public int getxSize() {
		return xSize;
	}

	@Override
	public int getySize() {
		return ySize;
	}

	@Override
	public double getxPos() {
		return  xPos;
	}

	@Override
	public double getyPos() {
		return yPos;
	}

	@Override
	public void setxSize(int s) {
		xSize = s;
	}

	@Override
	public void setySize(int s) {
		ySize = s;
	}

	@Override
	public void setxPos(int p) {
		xPos = p;
	}

	@Override
	public void setyPos(int p) {
		yPos = p;
	}

	@Override
	public void setxStartPos(int p) {
		xStartPos = p;
	}

	@Override
	public void setyStartPos(int p) {
		yStartPos = p;
	}
}

# FlatPormer
Personal project in figuring out how to make a versatile platform for a 2D platformgame and possible other applications.

# Controls
Arrow up, down, left, right moves the player cube.
R resets the game
Enter pauses the game

# Known Bugs
1. Collision on the right side of the cube is incorrect.
2. When the collision fails and the player enters the cube surface area the player gets ejected out on the wrong side.
3. Collision sometimes fails because of the speed of the player.
4. On collision the player cube bounces back under an unnatural angle.
